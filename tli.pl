use strict;
use warnings;
use feature qw( say );
use Browser::Start;

# cli
my $cmd = $ARGV[0] ;
say "you called : $cmd";
my $input = $ARGV[1] ; 

my $triple = $ARGV[2] ;

if($cmd=="i"){
    if($input == 'node@lts'){
        if($triple == "x64-windows-msi"){
            say "getting $input for $triple";
            open_url 'https://nodejs.org/dist/v16.13.2/node-v16.13.2-x64.msi';
            say "Remember to run the installer!";
            exit 0 ;
        }
        if($triple == "x32-windows-msi"){
            say "getting $input for $triple";
            open_url 'https://nodejs.org/dist/v16.13.2/node-v16.13.2-x32.msi';
            say "Remember to run the installer!";
            exit 0 ;
        }
    }
}
